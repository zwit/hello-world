# Test commit
This is a test commit from Standard Notes. A practice checklist is to follow.

- Test bullet (trying out markdown)
	- test sub-bullet

_what does this do?_ italicizes it

## And this?
Sub heading, apparently.

### Sub, sub heading?

#### Sub, sub, sub heading?

1. test1
2. test2
3. test3
	1. sub_test1
	2. sub_test2

**bold**

> Block quote!
> All day, apparently you can't force newline in a block quote. This shit wraps automatically.

> New block quote!

[Link](http://127.0.0.1)

What?! Apparently, the task list editor for SN uses the same task format as markdown? Sweet!

- [ ] Task to complete
- [ ] Another

Table?

Date   | Task
------ | -----
1/1/18 | Begin new year
2/1/18 | Begin February

---

Heading 1 | Heading 2 | Heading 3
--------- | --------- | ---------
Column 1  | Column 2  | Column 3
Row 2     | Row 2     | Row 2

Basic table

:+1:




